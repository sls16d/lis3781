# LIS3781 - Advanced Database Management

## Sydney Sawyer

### Assignment 2 Requirements:

*Three Parts:*

1. Provide screenshots of tables and insert statements
2. Provide screenshots of query result sets 
3. Populate tables both locally and remotely 

#### README.md file should include the following items:

* Screenshot of my SQL code
* Screenshot of my populated tables 
* Screenshot of my grant statements 


#### Assignment Screenshots:

*Company Table SQL Code*:

![Company SQL Code](img/lis3781_solutions_a.png)

*Populated Company Table*:

![Company Table](img/company_table.png)

*Customer Table SQL Code*:

![Customer SQL Code](img/lis3781_solutions_b.png)

*Populated Customer Table*:

![Customer Table](img/customer_table.png)



#### Tutorial Links:

*This Assignment - lis3781*:
[A2 for lis3781](https://bitbucket.org/sls16d/lis3781/src/master/ "A2 Link")