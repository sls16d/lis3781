/*
A character set is a set of symbols and encodings. 
A collation is a set of rules for comparing characters in a character set. 

Suppose that we have an alphabet with four letters: "A", "B", "a","b".
We give each letter a number: "A"=0, "B"=1, "a"=2, "b"=3.
The letter "A" is a symbol, the number 0 is the encoding for "A", and the combination of all four letters and their encodings is a character set. 

Suppose that we want to compare two string values, "A" and "B".
The simplest way to do this is to look at the encodings: 0 for "A" and 1 for "B"
Because 0 is less than 1, we say A is less than B. What we've just done is apply a collation to our character set. 
The collation is a set of rules (only one rule in this case): "compare encodings"
We call this simplest of all possible collations a binary collation.

*/

-- set foreign_key_checks=0;

drop database if exists sls16d;
create database if not exists sls16d;
use sls16d;

-- Table company 
DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
    cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip int(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
    cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,325,678.90',
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;


INSERT INTO company 
VALUES
(null,'C-Corp','115 Stuart Street','Boston','MA','021161003','6177728234','8932748.00',null,null,null),
(null,'S-Corp','275 Tremont Street','Boston','MA','021161001','6174261400','9912394.00',null,null,null),
(null,'Non-Profit-Corp','89 South Street','Brighton','MA','021111008','6174393142','1255443.00',null,null,null),
(null,'LLC','333 Terminal Street','Charlestown','MA','021291003','6175619111','55394.00',null,null,null),
(null,'Partnership','88 Broad Street','Boston','MA','021101002','6173670001','88886543.00',null,null,null);

SHOW WARNINGS; 


-- Table customer
DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
    cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_id INT UNSIGNED NOT NULL,
    cus_ssn binary(64) not null,
    cus_salt binary(64) not null,
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NULL,
    cus_city VARCHAR(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip int(9) unsigned ZEROFILL NULL,
    cus_phone bigint unsigned NOT NULL COMMENT 'ssn and zipcodes can be zero-filled, but not US area codes',
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) unsigned NULL COMMENT '12,345.67',
    cus_tot_sales DECIMAL(7,2) unsigned NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id),

    UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
    INDEX idx_cmp_id (cmp_id ASC),

    CONSTRAINT fk_customer_company
        FOREIGN KEY(cmp_id)
        REFERENCES company(cmp_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE 
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS; 

set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,1,unhex(SHA2(CONCAT(@salt,000456789),512)),@salt,'Discount','Lexi','Sway',null,'Boston','MA','021161003','6174269300',null,'1000.99','3000.00',null),
(null,2,unhex(SHA2(CONCAT(@salt,001456789),512)),@salt,'Loyal','Carter','Veal','130 Dartmouth Street','Boston','MA','021161001','6174425964',null,'23456.99','55000.00',null),
(null,3,unhex(SHA2(CONCAT(@salt,002456789),512)),@salt,'Impulse','Kelly','Dollar',null,'Boston','MA','021661003','8572637945',null,'22000.00','22000.00',null),
(null,4,unhex(SHA2(CONCAT(@salt,003456789),512)),@salt,'Need-Based','Wern','Osborne','1744 Washington Street','Boston','MA','021361003','6175360066',null,'200.00','1000.00',null),
(null,5,unhex(SHA2(CONCAT(@salt,004456789),512)),@salt,'Wandering','Denise','Martin','10 Post Street','Boston','MA','021461003','6173338776',null,'400.00','999.00',null);

SHOW WARNINGS;

select * from company; 
select * from customer;




