
INSERT INTO company 
VALUES
(null,'C-Corp','115 Stuart Street','Boston','MA','021161003','6177728234','8932748.00',null,null,null),
(null,'S-Corp','275 Tremont Street','Boston','MA','021161001','6174261400','9912394.00',null,null,null),
(null,'Non-Profit-Corp','89 South Street','Brighton','MA','021111008','6174393142','1255443.00',null,null,null),
(null,'LLC','333 Terminal Street','Charlestown','MA','021291003','6175619111','55394.00',null,null,null),
(null,'Partnership','88 Broad Street','Boston','MA','021101002','6173670001','88886543.00',null,null,null);

SHOW WARNINGS; 

set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,1,unhex(SHA2(CONCAT(@salt,000456789),512)),@salt,'Discount','Lexi','Sway',null,'Boston','MA','021161003','6174269300',null,'1000.99','3000.00',null),
(null,2,unhex(SHA2(CONCAT(@salt,001456789),512)),@salt,'Loyal','Carter','Veal','130 Dartmouth Street','Boston','MA','021161001','6174425964',null,'23456.99','55000.00',null),
(null,3,unhex(SHA2(CONCAT(@salt,002456789),512)),@salt,'Impulse','Kelly','Dollar',null,'Boston','MA','021661003','8572637945',null,'22000.00','22000.00',null),
(null,4,unhex(SHA2(CONCAT(@salt,003456789),512)),@salt,'Need-Based','Wern','Osborne','1744 Washington Street','Boston','MA','021361003','6175360066',null,'200.00','1000.00',null),
(null,5,unhex(SHA2(CONCAT(@salt,004456789),512)),@salt,'Wandering','Denise','Martin','10 Post Street','Boston','MA','021461003','6173338776',null,'400.00','999.00',null);

SHOW WARNINGS;

select * from company; 
select * from customer;