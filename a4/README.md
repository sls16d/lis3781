# LIS3781 - Advanced Database Management

## Sydney Sawyer

### Assignment 4 Requirements:

*Three Parts:*

1. Populate tables in MS SQL Server
2. Provide screenshot of ERD
3. Include unique data for all tables

#### README.md file should include the following items:

* Screenshot of my MS SQL Server ERD

#### Assignment Screenshots:

*MS SQL Server ERD*:

![ERD](img/erd.png)


#### Links:

*SQL Code*:
[SQL Code for A4](code/a4_statements.sql "SQL Code")

*This Assignment - lis3781*:
[Master for lis3781](https://bitbucket.org/sls16d/lis3781/src/master/ "Master Link")