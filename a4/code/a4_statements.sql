SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'sls16d')
DROP DATABASE sls16d;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'sls16d')
CREATE DATABASE sls16d;
GO

use sls16d;
GO

-- Table Person
IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL 
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person
(
    per_id SMALLINT not null identity(1,1),
    per_ssn binary(64) NULL,
    per_salt binary(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m','f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip int NOT NULL check(per_zip like '[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN('c','s')),
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

-- Table phone
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
    phn_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    phn_num bigint NOT NULL check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) NOT NULL CHECK (phn_type IN('h','c','w','f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    CONSTRAINT fk_phone_person
        FOREIGN KEY (per_id)
        REFERENCES dbo.person (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE 
);

-- Table customer
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer 
(
    per_id SMALLINT not null,
    cus_balance decimal(7,2) NOT NULL check (cus_balance >= 0),
    cus_total_sales decimal(7,2) NOT NULL check (cus_total_sales >= 0),
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
        FOREIGN KEY (per_id)
        REFERENCES dbo.person (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table slsrep
IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep 
(
    per_id SMALLINT not null,
    srp_yr_sales_goal decimal(8,2) NOT NULL check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) NOT NULL check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(8,2) NOT NULL check (srp_ytd_comm >= 0),
    srp_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
        FOREIGN KEY (per_id)
        REFERENCES dbo.person (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table srp_hist
IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
    
    sht_id SMALLINT not null identity(1,1),
    per_id SMALLINT not null,
    sht_type char(1) not null CHECK (sht_type IN('i','u','d')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) NOT NULL check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) NOT NULL check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) NOT NULL check (sht_yr_total_comm >= 0),
    sht_notes VARCHAR(255) NULL,
    PRIMARY KEY (sht_id),

    CONSTRAINT fk_srp_hist_slsrep
        FOREIGN KEY (per_id)
        REFERENCES dbo.slsrep (per_id)
        ON DELETE CASCADE 
        ON UPDATE CASCADE 
);

-- Table contact
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
    cnt_id int NOT NULL identity(1,1),
    per_cid smallint NOT NULL,
    per_sid smallint NOT NULL,
    cnt_date datetime NOT NULL,
    cnt_notes varchar(255) NULL,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
        FOREIGN KEY (per_cid)
        REFERENCES dbo.customer(per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,


    CONSTRAINT fk_contact_slsrep
        FOREIGN KEY (per_sid)
        REFERENCES dbo.slsrep (per_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

-- Table [order]
IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
    ord_id int NOT NULL identity(1,1),
    cnt_id int NOT NULL,
    ord_placed_data DATETIME NOT NULL,
    ord_filled_data DATETIME NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
        FOREIGN KEY (cnt_id)
        REFERENCES dbo.contact (cnt_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table store
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store 
(
    str_id SMALLINT NOT NULL identity(1,1),
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_city VARCHAR(30) NOT NULL,
    str_state CHAR(2) NOT NULL DEFAULT 'FL',
    str_zip int NOT NULL check(str_zip like '[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint NOT NULL check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id)
);

-- Table invoice 
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice 
(
    inv_id int NOT NULL identity(1,1),
    ord_id int NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL check (inv_total >= 0),
    inv_paid bit NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),

    CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC),

    CONSTRAINT fk_invoice_order
        FOREIGN KEY (ord_id)
        REFERENCES dbo.[order] (ord_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
        FOREIGN KEY (str_id)
        REFERENCES dbo.store (str_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table payment
IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment 
(
    pay_id int NOT NULL identity(1,1),
    inv_id int NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL check (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
        FOREIGN KEY (inv_id)
        REFERENCES dbo.invoice (inv_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table vendor
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor 
(
    ven_id SMALLINT NOT NULL identity(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip int NOT NULL check (ven_zip like '[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint NOT NULL check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NOT NULL,
    ven_url VARCHAR(100) NOT NULL,
    ven_notes VARCHAR(255) NULL,
    PRIMARY KEY (ven_id)
);


-- Table product
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
    pro_id SMALLINT NOT NULL identity(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NOT NULL,
    pro_weight FLOAT NOT NULL check (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL check (pro_qoh >= 0),
    pro_cost DECIMAL(7,2) NOT NULL check (pro_cost >= 0),
    pro_price DECIMAL(7,2) NOT NULL check (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
        FOREIGN KEY (ven_id)
        REFERENCES dbo.vendor (ven_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table product_hist
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist 
(
    pht_id int NOT NULL identity(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL check (pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL check (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product (pro_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- Table order_line
IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
    oin_id int NOT NULL identity(1,1),
    ord_id int NOT NULL,
    pro_id SMALLINT NOT NULL,
    oin_qty SMALLINT NOT NULL check (oin_qty >= 0),
    oin_price DECIMAL(7,2) NOT NULL check (oin_price >= 0),
    oin_notes VARCHAR(255) NULL,
    PRIMARY KEY (oin_id),

    CONSTRAINT fk_order_line_order
        FOREIGN KEY (ord_id)
        REFERENCES dbo.[order] (ord_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product (pro_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);


SELECT * FROM information_schema.tables;

INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES 
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 325502222, 'srogers@comcast.net', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 203208440, 'bwayne@knology.net', 's', NULL),
(3, NULL, 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram Street', 'New York', 'NY', 202862341, 'pparker@gmail.com', 's', NULL),
(4, NULL, 'Jane', 'Thompson', 'f', '1978-05-08', '13563 Ocean View Drive', 'Seattle', 'WA', 232084409, 'jthompson@gmail.com', 's', NULL),
(5, NULL, 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@yahoo.com', 's', NULL),
(6, NULL, 'Tony', 'Stark', 'm', '1972-05-04', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'stark@gmail.com', 'c', NULL),
(7, NULL, 'Hank', 'Pymi', 'm', '1980-08-28', '2355 Brown Street', 'Cleveland', 'OH', 222348890, 'hypm@aol.com', 'c', NULL),
(8, NULL, 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', 'c', NULL),
(9, NULL, 'Sandra', 'Dole', 'f', '1990-01-26', '87912 Lawrence Ave', 'Atlanta', 'GA', 202348890, 'sdole@gmail.com', 'c', NULL),
(10, NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', 'c', NULL),
(11, NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Avenue', 'Miami', 'FL', 672048823, 'acurry@gmail.com', 'c', NULL),
(12, NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 200219932, 'dprice@aol.com', 'c', NULL),
(13, NULL, 'Adam', 'Jurris', 'm', '1995-01-31', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 332048823, 'ajurris@gmail.com', 'c', NULL),
(14, NULL, 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct.', 'Billings', 'MT', 870219932, 'jsleen@gmail.com', 'c', NULL),
(15, NULL, 'Bill', 'Neiderheim', 'm', '1982-03-13', '43567 Netherland Blvd', 'South Bend ', 'IN', 320219932, 'bneider@aol.com', 'c', NULL);

select * from dbo.person; 

INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, NULL),
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);
select * from dbo.slsrep;

INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, NULL),
(9, 981.73, 1672.38, NULL),
(10, 541.23, 782.57, NULL);
select * from dbo.customer;

INSERT INTO dbo.contact
(per_cid, per_sid, cnt_date, cnt_notes)
VALUES
(6, 1, '1999-01-01', NULL),
(7, 2, '2001-01-01', NULL),
(8, 3, '2002-01-01', NULL),
(9, 4, '2003-01-01', NULL),
(10, 5, '2004-01-01', NULL);
select * from dbo.contact;

INSERT INTO dbo.[order]
(cnt_id, ord_placed_data, ord_filled_data, ord_notes)
VALUES
(1, '2010-11-12', '2010-12-12', NULL),
(2, '2011-11-12', '2011-12-12', NULL),
(3, '2012-11-12', '2012-12-12', NULL),
(4, '2013-11-12', '2013-12-12', NULL),
(5, '2014-11-12', '2014-12-12', NULL);
select * from dbo.[order];

INSERT INTO dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
('Walgreens', '112 Walnut Drive', 'Aspen', 'IL', 475328930,'2348849485', 'walgreens@gmail.com', 'walgreens.com', NULL),
('Walmart', '123 Peach Lane', 'Aspen', 'IL', 394758273,'2247485869', 'walmart@gmail.com', 'walmart.com', NULL),
('CVS', '333 Chicken Run', 'Clover', 'WA', 948503288,'3329384959', 'cvs@gmail.com', 'cvs.com', NULL),
('Lowes', '556 Alarm Street', 'Orlando', 'FL', 824859847,'9983948596', 'lowes@gmail.com', 'lowes.com', NULL),
('Dollar Tree', '981 Walton Street', 'Jacksonville', 'FL', 839485738,'9923844857', 'dollartree@gmail.com', 'dollartree.com', NULL);
select * from dbo.store;

INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(1, 1, '2001-01-01', 58.32, 0, NULL),
(2, 2, '2002-01-01', 100.59, 0, NULL),
(3, 3, '2003-01-01', 57.34, 1, NULL),
(4, 4, '2004-10-01', 537.29, 1, NULL),
(5, 5, '2005-9-01', 644.11, 1, NULL);
select * from dbo.invoice;

INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '112 Reese Lane', 'Orlando', 'FL', 383857837, '3384958594', 'sysco@gmail.com', 'sysco.com', NULL),
('General Electric', '84 Water Drive', 'Fort Myers', 'FL', 384748573, '3948548325', 'ge@gmail.com', 'ge.com', NULL),
('Cisco', '56 Coraline Street', 'Jacksonville', 'FL', 384736273, '4859483849', 'cisco@gmail.com', 'cisco.com', NULL),
('Goodyear', '99 Lease Place', 'DeLand', 'FL', 948732930, '2394958293', 'goodyear@gmail.com', 'goodyear.com', NULL),
('J&J', '990 Corn Drive', 'Sarasota', 'FL', 384948378, '4948574839', 'jandj@gmail.com', 'jandj.com', NULL);
select * from dbo.vendor;

INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', 'yellow handle', 2.5, 4.99, 7.99, 14.99, NULL, NULL),
(2, 'staple gun', 'sharp', 1.8, 1.99, 3.99, 6.00, NULL, NULL),
(3, 'pail', '16 gallons', 2.8, 3.89, 7.00, 10.99, NULL, NULL),
(4, 'oil', 'canola', 15, 19, 28.00, 40.00, NULL, NULL),
(5, 'pan', 'for frying', 3.5, 178, 3.00, 20.00, NULL, NULL);
select * from dbo.product;

INSERT INTO dbo.order_line
(ord_id, pro_id, oin_qty, oin_price, oin_notes)
VALUES
(1, 1, 10, 8.0, NULL),
(2, 2, 7, 9.0, NULL),
(3, 3, 3, 6.99, NULL),
(4, 4, 2, 12.76, NULL),
(5, 5, 12, 58.99, NULL);
select * from dbo.order_line;

INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(1, '2000-01-01', 5.99, NULL),
(2, '2001-01-01', 4.99, NULL),
(3, '2002-01-01', 8.48, NULL),
(4, '2003-10-10', 20.00, NULL),
(5, '2004-10-10', 9.33, NULL);
select * from dbo.payment;

INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 8.99, 30, NULL),
(2, '2006-01-02 11:53:34', 1.99, 3.99, 49, NULL),
(3, '2007-01-02 11:53:34', 3.89, 6.89, 88, NULL),
(4, '2008-01-02 11:53:34', 19.99, 22.99, 40, NULL),
(5, '2009-01-02 11:53:34', 13.99, 16.99, 11, NULL);
select * from dbo.product_hist;

INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDATE(), 100000, 110000, 11000, NULL),
(2, 'u', getDate(), SYSTEM_USER, getDATE(), 150000, 175000, 17500, NULL),
(3, 'i', getDate(), SYSTEM_USER, getDATE(), 200000, 185000, 18500, NULL),
(4, 'u', getDate(), SYSTEM_USER, getDATE(), 210000, 220000, 20000, NULL),
(5, 'i', getDate(), SYSTEM_USER, getDATE(), 225000, 230000, 2300, NULL);
select * from dbo.srp_hist;

INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(1, '3255022224', 'h', NULL),
(2, '2862341785', 'c', NULL),
(3, '5626383326', 'w', NULL),
(4, '3320488237', 'f', NULL),
(5, '3202199328', 'h', NULL);
select * from dbo.phone;


CREATE PROC dbo.CreatePersonSSN
AS
BEGIN

    DECLARE @salt binary(64);
    DECLARE @ran_num int;
    DECLARE @ssn binary(64);
    DECLARE @x INT, @y INT;
    SET @x = 1;

    SET @y=(select count(*) from dbo.person);

        WHILE (@x <= @y)
        BEGIN

        SET @salt=CRYPT_GEN_RANDOM(64);
        SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
        SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @rand_num));


        update dbo.person
        set per_ssn=@ssn, per_salt=@salt
        where per_id=@x;

        SET @x = @x + 1;

        END;

END;
GO

exec dbo.CreatePersonSSN