# LIS3781 - Advanced Database Management

## Sydney Sawyer

### Assignment 3 Requirements:

*Three Parts:*

1. Create and populate Oracle tables
2. Show your populated tables in Oracle environment 
3. Create SQL code for required reports 

#### README.md file should include the following items:

* Screenshot of my SQL code
* Screenshot of my populated tables 
 

#### Assignment Screenshots:

##### Creating Tables: 
|*SQL Code 1*: |*SQL Code 2*: 	|
|:-:	|:-:	|:-:    |
|![SQL1](img/sql_code1.png) 	|![SQL2](img/sql_code2.png) 	|

##### Table Inserts:
*Customer, Commodity, and "Order" Inserts*:

![Inserts](img/sql_inserts.png)


##### Query Results

*Customer query results*:

![Order query](img/customer_results.png)

*Commodity query results*:

![Commodity query](img/commodity_results.png)

*"order" query results*:

![Order query](img/order_results.png)


#### Tutorial Links:

*This Assignment - lis3781*:
[A3 for lis3781](https://bitbucket.org/sls16d/lis3781/src/master/ "A3 Link")