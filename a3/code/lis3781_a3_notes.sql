/*
How Oracle Enforces Data Integrity: through integrity constraints or triggers.

Integrity Constraints:
- NOT NULL: prohibitys a value from being null
- UNIQUE: values must be unique for all rows in the table
- PRIMARY KEY: uniquely identifies each table row 
- FOREIGN KEY: requires values in one table to match values in another table (or may be null). Referential integrity actions:
Update and Delete No Action
Delete CASCADE (Note: there is no 'update' cascade. Oracle believes PKs should be immutable, that is, never changing.)
Delete SET NULL 
- CHECK: requires value to comply with specified condition

For numeric columns, specify attribute as 'number' data type (case-insensitive)
Example: column_name NUMBER

Optionally, can also specify precision (total number of digits) and scale (number of digits to right of decimal point):
column_name NUMBER (precision, scale)

If precision not specified, column stores values as a given. If no scale specified, scale is zero. 
Oracle guarentees portability of numbers with a precision equal to or less than 38 digits 

Can specify scale and precision: 
column_name NUMBER (*, scale)
In this case, precision is 38, and whatever specified scale is maintained.
When specifying numeric fields, good idea to specify precision and scale. Provides extra integrity checking on input, as well as intuitive documentation.
*/

SET DEFINE OFF -- (Note: do not include semi-colon at end of line!) Also, test with first record in commodity table below

-- Notes: 
-- CASCADE CONSTRAINTS: specify CASCADE CONSTRAINTS to drop all referential integrity constraints that refer to primary and unique keys in dropped table.
-- If clause omitted, and such referential integrity constraints exist, database returns an error and does *NOT* drop table.

-- PURGE: unless PURGE clasue specified, DROP TABLE statement does *not* release memory back to tablespace for use by other objects 
-- Moreover, space continues to count toward user's space quota

--Note: DO *NOT* USE DROP/CREATE DATABASE COMMAND, AS YOUR TABLES ARE CREATED IN YOUR OWN TABLESPACE-- *NOT* DATABASE!
-- just connect to your schema
--2. create estore tables 
DROP SEQUENCE seq_cus_id; -- see Oracle documentation for auto increment
Create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;
-- create SEQUENCE seq_cus_id; -- default: starts with 1 and increments by 1


/*
Note:
VARCHAR is reserved by Oracle to support distinction between NULL and empty string in future, as ANSI standard prescribes 
VARCHAR2 does not distinguish between a NULL and empty string, and never will
If you rely on empty string and NULL being the same thing, you should use VARCHAR2

Oracle's Documentation: 
VARCHAR Datatype
The VARCHAR datatype is synonymous with the VARCHAR2 datatype.
To avoid possible changes in behavior, always use the VARCHAR2 datatype to store variable length character strings. 

Bottom-line:
Currently they are the same!
*HOWEVER*, VARCHAR *may* one day in the future have a different usage than VARCHAR2
Be safe--use VARCHAR2
*/

drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
cus_id      number(3,0) not null,
cus_fname   varchar2(15) not null,
cus_lname   varchar2(30) not null,
cus_street  varchar2(30) not null,
cus_city    varchar2(30) not null,
cus_state   char(2) not null,
cus_zip     number(9) not null,
cus_phone   number(10) not null,
cus_email   varchar2(100),
cus_balance number(7,2),
cus_notes   varchar2(255),
CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

DROP SEQUENCE seq_com_id; -- for auto increment
Create sequence seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
com_id      number not null,
com_name    varchar2(20),
com_price   NUMBER(8,2) NOT NULL,
cus_notes   varchar2(255),
CONSTRAINT pk_commodity PRIMARY KEY(com_id),
CONSTRAINT uq_com_name UNIQUE(com_name) -- Note: case-sensitive by default
);

/*
Note: 
Unique constraint (above) enforces data integrity--implicitly created a unique index as well
Unique index is for uniqueness and data retrieval performance 
Following statement explicitly creates unique index named ux_com_name for com_name attribute of the commodity table;
CREATE UNIQUE INDEX ux_com_name on commodity(com_name);

Indexes can be unique or non-unique: 
Unique indexes: guarantee that no two row of a table have duplicate values in column or columns
Non-unique indexesL do not impose this restriction-- though, do provide data retrieval performance

Can create non-unique indexes explicitly (i.e., outside of integrity constraints, strictly for retrieval performance):
Following statement creates index named idx_com_naem for com_name attribute fo commodity table:
CREATE INDEX idx_com_name on commodity(com_name);

Display names of all table constraints:
Must query data dictionary, specifically USER_CONS_COLUMNS view to see table columns and corresponding constraints:

SELECT * FROM user_cons_columns
WHERE table_name = 'commodity'; -- will NOT work!

SELECT * FROM user_cons_columns
WHERE table_name = 'COMMODITY'; -- will work

Legend: P, R, and C: 'primary key', 'referential integrity', and 'check constraint' (e.g., NOT NULL)

More information about table constraints, query USER_CONSTRAINTS view:
SELECT * FROM user_constraints 
WHERE table_name = '<your table name>';

More information about specific constraint, query USER_CONSTRAINTS view: 
SELECT * FROM user_constraints 
WHERE table_name = '<your table name>'
    AND constraint_name = '<your constraint name>';

-- Short constraints list: 
-- Note: SYS_C is system-generated name for constraint not explicitly named. Here: not-null check
select constraint_name, constraint_type
from user_constraints
where table_name = 'COMMODITY';

-- more constraint information
select uc.constraint_name, uc.constraint_type, ucc.column, ucc.position
from user_constraints uc
join user_cons_columns ucc on ucc.constraint_name = uc.constraint_name 
where uc.table_name = 'COMMODITY';

--short indexes list: 
select index_name, uniqueness
from user_indexes
where table_name = 'COMMODITY';

*/

DROP SEQUENCE seq_ord_id; -- for auto increment
Create sequence seq_ord_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

-- Demo purposes: Quoted identifiers can be reserved words (e.g., order), although this is *not* recommended
drop table "order" CASCADE CONSTRAINTS PURGE;
CREATE TABLE "order" 
(
ord_id          number(4,0) not null, -- max value 9999
cus_id          number,
com_id          number,
ord_num_units   number(5,0) NOT NULL,
ord_total_cost  number(8,2) NOT NULL,
ord_notes       varchar2(255),
CONSTRAINT pk_order PRIMARY KEY(ord_id),
CONSTRAINT fk_order_customer
FOREIGN KEY (cus_id)
REFERENCES customer(cus_id),
CONSTRAINT fk_order_commodity
FOREIGN KEY (com_id)
REFERENCES commodity(com_id),
CONSTRAINT check_unit CHECK (ord_num_units > 0),
CONSTRAINT check_total CHECK (ord_total_cost > 0)
);

-- Oracle NEXTVAL function used to retrieve next value in sequence 
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Ella', 'Churchill', '46 Hudson Street', 'Sun City', 'AZ', 85351, 2025550127, 'ella.churchill@gmail.com', 222.55, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Jake', 'Jackson', '8428 Wellington Drive', 'San Diego', 'CA', 92111, 2025550192, 'jake.jackson@gmail.com', 12.23, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Harry', 'Davidson', '22 Philmont Lane', 'Grand Blanc', 'MI', 48439, 2395556798, 'harryd@gmail.com', 1000.43, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Sonia', 'Page', '402 Bishop Street', 'Linden', 'NJ', 07036, 8685553421, 'spage22@gmail.com', 88.99, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Victor', 'Gibson', '1200 Mill Pond St.', 'Chatsworth', 'GA', 30705, 7734562375, 'v.gibson@gmail.com', 500.60, NULL);
commit;

-- Note: Oracle does not auto commit by default! DML STATEMENTS WILL ONLY LAST FOR THE SESSION!
-- When forgetting to commit DML statements--for example, with inserts, selecting a table will display "no rows selected"!

INSERT INTO commodity VALUES (seq_com_id.nextval, 'Froot Loops', 12.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Connect 4', 22.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Pepto', 3.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Red Vines', 2.25, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Sprite', 3.12, NULL);
commit; 

INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 50, 200, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 30, 100, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 6, 654, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 24, 972, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 7, 300, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 5, 15, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 40, 57, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 4, 300, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 14, 770, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 15, 883, NULL);
commit;

select * from customer; 
select * from commodity; 
select * from "order";



