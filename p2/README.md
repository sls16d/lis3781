# LIS3781 - Advanced Database Management

## Sydney Sawyer

### Project 2 Requirements:

*Three Parts:*

1. Download MongoDB through command line
2. Importy primer-dataset.json
3. Use MongoDB commands to fufill the required reports 

#### README.md file should include the following items:

* Screenshot at least one MongoDB commands
* Link JSON code for required reports 
* .mwb and .sql links to SQL code and ERD 
 

##### MongoDB Commands:
*1) Display all documents in the collection*:
**db.restaurants.find();**

*Results*:
![Q1 Code](img/q1.png)

*2) Display the number of documents in collection*:
**db.restaurants.find().count()**

*Results*:
![Q2 Code](img/q2.png)

*3) Retrieve the first five documents*:
**db.restaurants.find().limit(5).pretty()**

*Results*:
![Q3 Code](img/q3.png)


*4) Retrieve restaurants in the Brooklyn borough.*:
**db.restaurants.find( { "borough": "Brooklyn"})**

*5) Retrieve restaurants whose cuisine is American.*:
**db.restaurants.find( { "cuisine": "American "})**

*6) Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers.*:
**db.restaurants.find( { "borough": "Manhattan", "cuisine": "Hamburgers"})**

*7) Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers .*:
**db.restaurants.find( { "borough": "Manhattan", "cuisine": "Hamburgers"}).count()**

*8) Query zipcode field in embedded address document, and retrieve restaurants in the 10075 zipcode area.*:
**db.restaurants.find( { "address.zipcode": "10075"})**

*9) Retrieve restaurants whose cuisine is chicken and whose zip code is 10024..*:
**db.restaurants.find( { "cuisine": "Chicken", "address.zipcode": "10024"})**

*10) Retrieve restaurants whose cuisine is chicken or whose zip code is 10024..*:
**db.restaurants.find( { $or: [{ "cuisine": "Chicken"}, { "address.zipcode": "10024"}]})**

*11) Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sortby descending order of zipcode.*:
**db.restaurants.find( { "borough": "Queens", "cuisine": "Jewish/Kosher"}).sort({"address.zipcode": -1})**

*12) Retrieve restaurants with a grade A.*:
**db.restaurants.find( { "grades.grade": "A" })**

*13) Retrieve restaurants with a grade A, displaying only collectionid, restaurant name, and grade.*:
**db.restaurants.find( { "grades.grade": "A"}, { "name": 1, "grades.grade": 1})**

*14) Retrieve restaurants with a grade A, displaying only restaurant name, and grade(no collection id).*:
**db.restaurants.find( { "grades.grade": "A"}, { "name": 1, "grades.grade": 1, _id: 0})**

*15) Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending.*:
**db.restaurants.find( { "grades.grade": "A"}).sort({ "cuisine": 1, "address.zipcode": -1})**

*16) Retrieve restaurants with a score higher than 80.*:
**db.restaurants.find( { "grades.score": {$gt: 80}})**

*17) Insert a record with the following data:street = 7th Avenuezip code = 10024building = 1000coord = -58.9557413, 31.7720266borough = Brooklyncuisine = BBQdate = 2015-11-05T00:00:00Zgrade" : Cscore = 15name = Big Texrestaurant_id = 61704627.*:

db.restaurants.insert(
    {
        "address" : {
            "street" : "7th Avenue",
            "zipcode" : "10024",
            "building" : "1000",
            "coord" : [-58.9557413, 31.7720266],
        },
        "borough" : "Brooklyn",
        "cuisine" : "BBQ",
        "grades" : [
            {
                "date" : ISODate('2015-11-05T00:00:00Z'),
                "grade" : "C",
                "score" : 15
            }
        ],
        "name" : "Big Tex",
        "restaurant_id" : "61704627"
    }
)
db.restaurants.find().sort({_id:-1}).limit(1).pretty();

*18) Update the following record:Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update the lastModified field with the current date.*:
**db.restaurants.find( { "name": "White Castle" }, { _id:1}).limit(1)**

**db.restaurants.update( {"_id": ObjectId("6071219d482f9212313da2b5") },{$set: { "cuisine": "Steak and Sea Food"}, $currentDate: { "lastModified": true}})**

**db.restaurants.find( { "name": "White Castle" }).limit(1).pretty()**

*Results*:
![Q18 Code](img/q18.png)

*19) Delete the following records:Delete all White Castle restaurants.*:
**db.restaurants.remove({ "name": "White Castle"})**

*Results*:
![Q19 Code](img/q19.png)


#### Links:
*Required JSON code - lis3781*:
[JSON Code](code/mongo.json "JSON code link")

*Main README.md - lis3781*:
[MAIN README for lis3781](https://bitbucket.org/sls16d/lis3781/src/master/ "Main README Link")
