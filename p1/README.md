# LIS3781 - Advanced Database Management

## Sydney Sawyer

### Project 1 Requirements:

*Three Parts:*

1. Create an ERD following required business rules
2. Forward-engineer your ERD to the CCI server 
3. Create SQL statements to retrieve data asked for in project 

#### README.md file should include the following items:

* Screenshot of my ERD
* Screenshot of my populated tables
* .mwb and .sql links to SQL code and ERD 
 
#### Business Rules:
> 
> As the lead DBA for a local municipality, you are contacted by the city council to design a database in order to track and document the city's court case data. Some report examples:
>
> Which attorney is assigned to what case(s)?
>
> How many unique clients have cases(*be sure* to add a client to more than one case)?
>
> How many cases has each attorney been assigned, and the names of their clients (return number and names)?
> 
> How many cases does each client have with the firm (return name and number value)?
>
> Which types of cases does/did each client have/had and their start/end dates?
>
> Which attorney is associated to which client(s), and to which case(s)?
>
> Names of three judges with the most number of years in practice, include number of years.
>
> Also, include these business rules:
>
>> - An attorney is retained by (or assigned to) one or more clients, for each case.
>>
>> - A client has (or is assigned to) one or more attorneys for each case.
>>
>> - An attorney has one or more cases.
>>
>> - A client has one or more cases.
>>
>> - Each court has one or more judges adjudicating.
>>
>> - Each judge adjudicates upon exactly one court.
>>
>> - Each judge may preside over more than one case.
>>
>> - Each case that goes to court is presided over by exactly one judge.
>>
>> - **A person can have more than one phone number**
>
>

#### Project Screenshots:

##### ERD:
![ERD](img/ERD.png)

##### SQL Statements & Solutions:
*1) Create a view that displays attorneys' full names, full addresses, ages, hourly rates, the bar names they have passed, as well as their specialties, sort by attorneys' last names*:

*SQL Code*:
![Q1 Code](img/q1_c.png)

*Table*:
![q](img/q1.png) 

*2) create a stored procedure that displays how many judges were born in each month of the year, sorted by month.*:

*SQL Code*:
![Q2 Code](img/q2_c.png)

*Table*: 
![SQL2](img/q2.png)

*3) Create a stored procedure that displays all case types and descriptions, as well as judges' full names, full addresses, phone numbers, years in practice, for cases they presided over, with their start and end dates, sort by judges' last names.*:

*SQL Code*:
![Q3 Code](img/q3_c.png)

*Tables*: 
![Q3](img/q3.png)

*4) Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table.*:

*SQL Code*:
![Q4 Code](img/q4_c.png)

|*Judge Table*: |*Judge History Table*: 	|
|:-:	|:-:	|:-:    |
| ![Q4](img/q4_p1.png) 	|![Q4](img/q4_p2.png) 	|

*5) Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table*:

*SQL Code*:
![Q5 Code](img/q5_c.png)

|*Judge Table*: |*Judge History Table*: 	|
|:-:	|:-:	|:-:    |
| ![Q5](img/q5_p1.png) 	|![SQL2](img/q5_p2.png) 	|

*6) Create a one-time event that executes one hour following its creation, the event shouls add a judge record (one more than the required 5 records), have the event call a stored procedure that adds the record one_time_add_judge.*:

*SQL Code*:
![Q6 Code](img/q6_c.png)

|*Judge Table*: |*Judge History Table*: 	|
|:-:	|:-:	|:-:    |
| ![Q6](img/q6_p1.png) 	|![Q6](img/q6_p2.png) 	|


*EXTRA CREDIT - Create a scheduled event that will run every two months, beginning in three weeks, and runs for the next four years, starting from the creation date. The event should not allow more than the first 100 judge histories to be stored, thereby removing all others (name it remove_judge_history)*:

*SQL Code*:
![Extra Credit](img/ec.png)

#### Links:

*Main README.md - lis3781*:
[MAIN README for lis3781](https://bitbucket.org/sls16d/lis3781/src/master/ "Main README Link")

*Project 1 .mwb file*:
[.mwb](code/project1.mwb ".mwb")

*Project 1 - .sql file*:
[.sql](code/lis3781_p1_solutions.sql ".sql")