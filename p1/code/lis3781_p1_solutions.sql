/* 1) Create a view that displays attorneys' full names, full addresses, ages, hourly rates, the bar names they have passed, as well as their specialties, sort by attorneys' last names. */

drop VIEW if exists v_attorney_info;
CREATE VIEW v_attorney_info AS 

select 
concat(per_lname, ', ', per_fname) as name,
concat(per_street, ', ', per_city, ', ', per_state, ' ', per_zip) as address,
TIMESTAMPDIFF(year, per_dob, now()) as age, 
CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
bar_name, spc_type
from person
    natural join attorney
    natural join bar
    natural join specialty
    order by per_lname;

select 'display view v_attorney_info' as '';

select * from v_attorney_info;
drop VIEW if exists v_attorney_info;
do sleep(5);



/* 2) create a stored procedure that displays how many judges were born in each month of the year, sorted by month. */

select 'Step a) Display all persons DOB months; testing MySQL monthname() function' as '';

select per_id, per_fname, per_lname, per_dob, monthname(per_dob) from person;
do sleep(5);

select 'Step b) Display pertinent judge data' as '';

select p.per_id, per_fname, per_lname, per_dob, per_type
from person as p 
natural join judge as j;
do sleep(5);

select 'Final: Step c) Stored proc: Display month numbers, month names, and how many judges were born each month' as '';

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
CREATE PROCEDURE sp_num_judges_born_by_month()
BEGIN
    select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
    from person
    natural join judge
    group by month_name, month
    order by month;
END //
DELIMITER ;

select 'calling sp_num_judges_born_by_month' as '';

CALL sp_num_judges_born_by_month();
do sleep(5);

drop procedure if exists sp_num_judges_born_by_month;
do sleep(5);

/* 3) Create a stored procedure that displays all case types and descriptions, as well as judges' full names, full addresses, phone numbers, years in practice, for cases they presided over, with their start and end dates, sort by judges' last names. */

drop procedure if exists sp_cases_and_judges;
DELIMITER //
CREATE PROCEDURE sp_cases_and_judges()
BEGIN

select per_id, cse_id, cse_type, cse_description,
concat(per_fname, ', ', per_lname) as name,
concat('(', substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
phn_type, jud_years_in_practice, cse_start_date, cse_end_date
from person 
natural join judge
natural join `case`
natural join phone
where per_type='j'
order by per_lname;

END //
DELIMITER ;

CALL sp_cases_and_judges();
do sleep(5);
drop procedure if exists sp_cases_and_judges;

/* 4) Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table. */

select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' as''; 
do sleep(5);

select 'show person data *before* adding person record' as '';
select per_id, per_fname, per_lname from person; 
do sleep(5);

SET @salt=RANDOM_BYTES(64);
SET @num=000000000;
SET @ssn=unhex(sha2(concat(@salt,@num), 512));

INSERT INTO person 
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge');

select 'show person data *after* adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(5);

DROP TRIGGER IF EXISTS trg_judge_history_after_insert
DELIMITER //
CREATE TRIGGER trg_judge_history_after_insert
AFTER INSERT ON judge
FOR EACH ROW
BEGIN

INSERT INTO judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(
    NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary, 
    concat('modifying user: ', user(), 'Notes: ', NEW.jud_notes)
);
END //
DELIMITER ;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
((select max(per_id)from person where per_type='j'), 3, 175000, 31, 'transferred from neighboring jurisdiction');

select * from judge;
select * from judge_hist;
do sleep(7);

/* 5) Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table */

DROP TRIGGER IF EXISTS trg_judge_after_update;
DELIMITER //
CREATE TRIGGER trg_judge_after_update
AFTER UPDATE ON judge 
FOR EACH ROW 
BEGIN

INSERT INTO judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(
    NEW.per_id, NEW.crt_id, current_timestamp(), 'u', NEW.jud_salary, 
    concat('modifying user: ', user(), 'Notes: ', NEW.jud_notes)
);
END //
DELIMITER ;

UPDATE judge
SET jud_salary=190000, jud_notes='senior justice - longest serving member'
WHERE per_id=16;

select * from judge;
select * from judge_hist;
do sleep(7);

DROP TRIGGER IF EXISTS trg_judge_after_update;


/* 6) Create a one-time event that executes one hour following its creation, the event shouls add a judge record (one more than the required 5 records), have the event call a stored procedure that adds the record one_time_add_judge. */

DROP PROCEDURE IF EXISTS sp_add_judge_record;
DELIMITER //

CREATE PROCEDURE sp_add_judge_record()
BEGIN
    INSERT INTO judge
    (per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
    VALUES
    (6, 1, 110000, 0, concat('New judge was former attorney.', 'Modifying event creator: ', current_user()));
END //
DELIMITER ;

show variables LIKE'event_scheduler';
do sleep(5);

select * from judge;
select * from judge_hist;
do sleep(7);

SET GLOBAL event_scheduler = ON;

DROP EVENT IF EXISTS one_time_add_judge
DELIMITER //
CREATE EVENT IF NOT EXISTS one_time_add_judge
ON SCHEDULE 
    AT NOW() + INTERVAL 5 SECOND 
COMMENT 'adds a judge record only one time'
DO 
BEGIN
    CALL sp_add_judge_record();
END //
DELIMITER ;

SHOW EVENTS FROM sls16d;
do sleep(5);



/* EXTRA CREDIT */

DROP EVENT IF EXISTS remove_judge_history;

DELIMITER //
CREATE EVENT IF NOT EXISTS remove_judge_history
ON SCHEDULE
    EVERY 2 MONTH 
STARTS NOW() + INTERVAL 3 WEEK
ENDS NOW() + INTERVAL 4 YEAR 
COMMENT 'keeps only the first 100 judge records'
DO
BEGIN 
    DELETE FROM judge_hist where jhs_id > 100;
END //
DELIMITER ;

DROP EVENT IF EXISTS remove_judge_history;



20:18:14	drop VIEW if exists v_attorney_info	0 row(s) affected, 1 warning(s): 1051 Unknown table 'sls16d.v_attorney_info'	0.0043 sec
