# LIS3781 - Advanced Database Management

## Sydney Sawyer

### Assignment 5 Requirements:

*Three Parts:*

1. Following business rules create tables in MS SQL Server
2. Include 25 unique records in each table
3. Create SQL statements to retrieve data asked for in project 

#### README.md file should include the following items:

* Screenshot of my ERD
* SQL code for the required reports 
 
> #### Business Rules:
> 
> Expanding upon the *high-volume* home office supply company's data tracking requirements, the CFO requests your services again to extend the data model's functionality.
> The CFO has read books about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart test platform. 
> He is under pressure from the members of the company's board of directors who want to review more detailed *sales* reports based upon the following measurements:
>
>> 1. Product
>> 2. Customer
>> 3. Sales representatives
>> 4. Time(year, quarter, month, week, day, time)
>> 5. Location
>
> Furthermore, the board members want location to be expanded to include the following characteristics of location:
>
>> 1. Region
>> 2. State
>> 3. City
>> 4. Store
>
>
#### Assignment Screenshots:

##### ERD:
![ERD](img/A5_ERD.png)

##### SQL Statements & Solutions:
*1) Create a stored procedure (product_days_of_week) listing the product names, descriptions, and the day of the week in which they were sold, in ascending order of the day of the week*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![Q1 Code](img/q1_code.png) 	|![Q1 Result](img/q1_result.png) 	|


*2) Create a stored procedure (product_drill_down) listing the product name, quantity on hand, store name, city name, state name, and region name where each product was purchased, in descending order of quantity on hand.*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![Q2 Code](img/q2_code.png) 	|![Q2 Result](img/q2_result.png) 	|


*3) Create a stored procedure (add_payment) that adds a payment record. Use variables and pass suitable arguments.*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![Q3 Code](img/q3_code.png) 	|![Q3 Result](img/q3_result.png) 	|

*4) Create a stored procedure (customer_balance) listing the customer's id, name, invoice id, total paid on invoice, balance (derived attribute from the difference of a customer's invoice total and their respective payments), pass customer's last name as an argument-- which may return more than one value.*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![Q4 Code](img/q4_code.png) 	|![Q4 Result](img/q4_result.png) 	|

*5) Create and display the results of a stored procedure (store_sales_between_dates) that lists each store's id, sum of total sales (formatted), and years for a given time period, by passing start/end dates, group by years, and sort by total sales then years, both in descending order.*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![Q5 Code](img/q5_code.png) 	|![Q5 Result](img/q5_result.png) 	|

*6) Create a trigger (trg_check_inv_paid) that updates an invoice record, after a payment has been made, indicating whether or not the invoice has been paid.*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![Q6 Code](img/q6_code.png) 	|![Q6 Result](img/q6_result.png) 	|


*EXTRA CREDIT - Create and display the results of a stored procedure (order_line_total) that calculates the total price for each order line, based upon the product price times quantity, which yields a subtotal (oln_price), total column includes 6% sales tax. Query result set should display order line id, product id, name, description, price, order line quantity, subtotal (oln_price), and total with 6% sales tax. Sort by product id.*:

|*SQL Code*: |*SQL Code Result*: 	|
|:-:	|:-:	|:-:    |
|![EC Code](img/ec_code.png) 	|![EC Result](img/ec_result.png) 	|

#### Links:

*Main README.md - lis3781*:
[MAIN README for lis3781](https://bitbucket.org/sls16d/lis3781/src/master/ "Main README Link")

*A5 Code - .sql file*:
[.sql](code/a5_code.sql ".sql")