# LIS 3781 - Advanced Database Management

## Sydney Sawyer

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations 
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git commands with descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Provide screenshots of tables and insert statements
    - Provide screenshots of query result sets and grant statements 
    - Populate tables both locally and remotely 
3. [A3 README.MD](a3/README.md "My A3 README.md file")
    - Login to Oracle using RemoteLabs
    - Create and populate tables in Oracle
    - Provide screenshots of query results
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Populate tables using MS SQL Server 
    - Include uniqe data entries for each table
    - Create an ERD
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Populate tables using MS SQL Server
    - Use A4 tables and add tables
    - Update A4 code 
    - Create an ERD

### Project Requirements: 

1. [P1 README.MD](p1/README.md "My P1 README.md file")
    - Create tables and forward engineer to the CCI server
    - Use SHA2 hashing with salt on SSNs in the person table
    - Create an ERD 
2. [P2 README.MD](p2/README.md "My P2 README.md file")
    - Install MongoDB
    - Import data primer-dataset.json to bin directory
    - Use commands to fufill the required reports 